{
    "id": "cee15b14-29a8-4b54-bdb7-6dccf1e2cb1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_wire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 128,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55b37765-0884-45ea-8994-8d5d5deb1a58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee15b14-29a8-4b54-bdb7-6dccf1e2cb1a",
            "compositeImage": {
                "id": "086683eb-7d0a-4c90-8857-b6db8587ee43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55b37765-0884-45ea-8994-8d5d5deb1a58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe6113d7-8c9f-4409-9b98-9c75793c0819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55b37765-0884-45ea-8994-8d5d5deb1a58",
                    "LayerId": "b548490a-e9f7-4608-b0bb-39e5ab641494"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b548490a-e9f7-4608-b0bb-39e5ab641494",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cee15b14-29a8-4b54-bdb7-6dccf1e2cb1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 192,
    "yorig": 64
}