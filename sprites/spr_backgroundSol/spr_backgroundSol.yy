{
    "id": "dc9b0b2a-0477-490e-b944-5eafdbb000fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backgroundSol",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 128,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "921ae301-1cdc-475c-9508-e51185cdd08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc9b0b2a-0477-490e-b944-5eafdbb000fd",
            "compositeImage": {
                "id": "0dc7d122-ac2c-4817-baed-fc5b77b134fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "921ae301-1cdc-475c-9508-e51185cdd08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e554cd2-ec4c-44d8-927a-34e8232e5b54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "921ae301-1cdc-475c-9508-e51185cdd08a",
                    "LayerId": "644ac6a3-659e-4794-beaa-6a7dab01924c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "644ac6a3-659e-4794-beaa-6a7dab01924c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc9b0b2a-0477-490e-b944-5eafdbb000fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}