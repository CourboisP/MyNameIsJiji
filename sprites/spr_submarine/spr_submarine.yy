{
    "id": "76136a1a-dbe5-4776-8491-9f24d5a51278",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_submarine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 16,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f659b1af-87d2-4f8d-9ea7-404c1813a452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76136a1a-dbe5-4776-8491-9f24d5a51278",
            "compositeImage": {
                "id": "f6acb297-aee9-4fa3-892a-c9c772341b9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f659b1af-87d2-4f8d-9ea7-404c1813a452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1704315-ab50-406d-9675-25143b337ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f659b1af-87d2-4f8d-9ea7-404c1813a452",
                    "LayerId": "eae58eff-a9d8-46b5-a655-7ef7a8f1dc08"
                }
            ]
        },
        {
            "id": "e4647ff4-75e9-4bae-af8a-7842b635cbab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76136a1a-dbe5-4776-8491-9f24d5a51278",
            "compositeImage": {
                "id": "7825bc43-0fe5-4d42-8ca0-9bd02ead8b56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4647ff4-75e9-4bae-af8a-7842b635cbab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa3a53ae-c487-495d-ba8b-e044365516c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4647ff4-75e9-4bae-af8a-7842b635cbab",
                    "LayerId": "eae58eff-a9d8-46b5-a655-7ef7a8f1dc08"
                }
            ]
        },
        {
            "id": "2238cf14-dc88-4774-abb9-b5e260d0c9b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76136a1a-dbe5-4776-8491-9f24d5a51278",
            "compositeImage": {
                "id": "25c1bb30-f299-4d73-89d4-474e808cac20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2238cf14-dc88-4774-abb9-b5e260d0c9b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c404eb5-28b0-49ad-832c-eab4276d53a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2238cf14-dc88-4774-abb9-b5e260d0c9b5",
                    "LayerId": "eae58eff-a9d8-46b5-a655-7ef7a8f1dc08"
                }
            ]
        },
        {
            "id": "4f8c6b56-8e54-4933-a41c-1aa38812ea61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76136a1a-dbe5-4776-8491-9f24d5a51278",
            "compositeImage": {
                "id": "8b6510ff-274c-463f-9729-d933a672ffea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8c6b56-8e54-4933-a41c-1aa38812ea61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f3e8784-b020-4381-88b3-02b4048cc617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8c6b56-8e54-4933-a41c-1aa38812ea61",
                    "LayerId": "eae58eff-a9d8-46b5-a655-7ef7a8f1dc08"
                }
            ]
        },
        {
            "id": "23745a01-8a79-4f56-9743-1f1f7b5af556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76136a1a-dbe5-4776-8491-9f24d5a51278",
            "compositeImage": {
                "id": "72f22e0f-e994-4d31-a0e3-585b170967c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23745a01-8a79-4f56-9743-1f1f7b5af556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e2f4b64-058e-49ee-9b29-12d99e2b17ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23745a01-8a79-4f56-9743-1f1f7b5af556",
                    "LayerId": "eae58eff-a9d8-46b5-a655-7ef7a8f1dc08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "eae58eff-a9d8-46b5-a655-7ef7a8f1dc08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76136a1a-dbe5-4776-8491-9f24d5a51278",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 128
}