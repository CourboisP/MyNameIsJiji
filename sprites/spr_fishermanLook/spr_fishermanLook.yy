{
    "id": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fishermanLook",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 64,
    "bbox_right": 127,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43e403ec-9bee-4dd2-9295-b72671be6261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "df36ca98-6a4e-40c1-99b8-770b70acab99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43e403ec-9bee-4dd2-9295-b72671be6261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e498a51-b091-47bb-ad32-580f2ca84bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43e403ec-9bee-4dd2-9295-b72671be6261",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "b4465a58-e976-402d-9c8c-eb8230baaf84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "2c5d6f4c-3a7f-4001-8f56-6a1ac129ca20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4465a58-e976-402d-9c8c-eb8230baaf84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c87a3fd2-c24f-4c66-8c29-be2a49e7e5ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4465a58-e976-402d-9c8c-eb8230baaf84",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "593e26ee-9a45-4378-9362-a3e0160d23b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "532725de-eab0-41c8-b728-ffc46082fa01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "593e26ee-9a45-4378-9362-a3e0160d23b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06b933f2-900c-4251-8ee0-33725f28976a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "593e26ee-9a45-4378-9362-a3e0160d23b0",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "8e8d707b-72af-4022-a516-dcf787c6d197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "6c1706fa-8870-4f52-99a6-f1ca2c1e3190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e8d707b-72af-4022-a516-dcf787c6d197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4550eb5-b601-4bcf-871b-7efb618fed67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e8d707b-72af-4022-a516-dcf787c6d197",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "0879f5d0-91bb-440e-87c8-f56829231091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "9dd55984-1940-46b5-ad62-1d34ed4b9470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0879f5d0-91bb-440e-87c8-f56829231091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b9eaf5-b503-4477-b688-2072625eb16c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0879f5d0-91bb-440e-87c8-f56829231091",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "3c453764-c872-48ad-979c-ecace8a5c86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "1e8e3341-17c4-4abf-a6b4-750f20e958bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c453764-c872-48ad-979c-ecace8a5c86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d81f07b9-25b3-4abb-8c28-80dc0aa85e68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c453764-c872-48ad-979c-ecace8a5c86e",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "c426c4c6-5f19-4d31-b1f0-ee5a13a84c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "b46b1bf1-67ac-49ff-b0d3-67e0ce10ce6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c426c4c6-5f19-4d31-b1f0-ee5a13a84c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b466a419-2dea-4e72-b415-c96f862f79b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c426c4c6-5f19-4d31-b1f0-ee5a13a84c35",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "1ddb533d-3a6f-4682-a1b1-f8b72fa2daeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "76c6372f-147f-47e1-90b0-1922bf5680c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ddb533d-3a6f-4682-a1b1-f8b72fa2daeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4961aed-7937-4d28-8021-8efccda87702",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ddb533d-3a6f-4682-a1b1-f8b72fa2daeb",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "73f5734f-d5f3-47fb-8a78-adbb44bc19dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "9afce6d8-a68a-4b50-96fa-92e691e132f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f5734f-d5f3-47fb-8a78-adbb44bc19dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a522a3-e143-40fe-b5c0-ca4d01cf156f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f5734f-d5f3-47fb-8a78-adbb44bc19dc",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "f1abe921-3c13-402d-a7c1-2b5835d8b83a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "39991aaa-c4f7-45c8-8fe5-e4b4d959bad3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1abe921-3c13-402d-a7c1-2b5835d8b83a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f67e1c0-f7e9-4c7b-bfbe-c51a600914e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1abe921-3c13-402d-a7c1-2b5835d8b83a",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "3237a37c-cbd5-481b-935b-55855d08bd19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "ed136993-d8fb-44ab-b8d5-a7e76a882907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3237a37c-cbd5-481b-935b-55855d08bd19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5bbebb9-0c8f-42c9-837f-e24b576f62df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3237a37c-cbd5-481b-935b-55855d08bd19",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "c8452654-8d1f-46ec-8872-7a18d046634e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "254dd054-725a-4152-9028-307ae7038ddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8452654-8d1f-46ec-8872-7a18d046634e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f8f519b-e73a-4749-8408-fc024e97cdc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8452654-8d1f-46ec-8872-7a18d046634e",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        },
        {
            "id": "2b8a3bfe-bdb8-4b60-b988-4241ed69ed15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "compositeImage": {
                "id": "9b823ab7-d7e6-4900-801f-975157f76054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b8a3bfe-bdb8-4b60-b988-4241ed69ed15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8298ce65-2ec7-425a-8ea8-5c945405470a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b8a3bfe-bdb8-4b60-b988-4241ed69ed15",
                    "LayerId": "7d6b562f-55c4-442e-862c-7ca524931f2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7d6b562f-55c4-442e-862c-7ca524931f2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c9d1361-006b-439c-9c5e-a7799d1a064d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}