{
    "id": "5205bb0f-bf54-48f1-a2a4-9964fd5aa628",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fade_to_the_end",
    "eventList": [
        {
            "id": "93911784-d359-467d-a7fd-b389d35120c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5205bb0f-bf54-48f1-a2a4-9964fd5aa628"
        },
        {
            "id": "6e84ee1a-826c-4028-b054-0a1c7ce886ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5205bb0f-bf54-48f1-a2a4-9964fd5aa628"
        },
        {
            "id": "7c038292-89ba-4b7b-a604-2dbdfc2e72b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5205bb0f-bf54-48f1-a2a4-9964fd5aa628"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}