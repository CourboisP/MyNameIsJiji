{
    "id": "b1c314f1-f590-4ee9-a9c2-89d7421b0b2d",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_fishes",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "79559b7c-1750-4898-85bc-b7d7e6f12aa8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": -48.63672,
            "y": 1239.13208,
            "speed": 100
        },
        {
            "id": "d15b2dfa-5978-405e-8a35-96818cf7188f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 132.495361,
            "y": 1278.75476,
            "speed": 100
        },
        {
            "id": "60afc1be-449a-4244-bbbc-3597d57397f8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 236.268982,
            "y": 1214.60376,
            "speed": 100
        },
        {
            "id": "cf568451-ad51-4caf-a4bb-74468a5dc71c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 309.853882,
            "y": 1237.24536,
            "speed": 100
        },
        {
            "id": "7ad758ae-76e4-45e4-9b0c-0830374460e2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 379.665161,
            "y": 1214.60376,
            "speed": 100
        },
        {
            "id": "9ba7f73e-af7b-4475-aa2e-3d2ba57e1f05",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 444.929321,
            "y": 1241.13208,
            "speed": 100
        },
        {
            "id": "49d2053a-1eaa-4dcf-8529-e76aa9af327c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 509.853882,
            "y": 1224.03784,
            "speed": 100
        },
        {
            "id": "df9194c4-9522-4fb1-86b0-0b29910e5ac9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 598.646362,
            "y": 1237.24536,
            "speed": 100
        },
        {
            "id": "11151db2-4482-4974-b7e8-1b193ed3d77e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 628.7218,
            "y": 1259.88684,
            "speed": 100
        },
        {
            "id": "ad8a4043-4bb5-474c-9a43-4ef1bf93bda5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 690.985962,
            "y": 1252.3396,
            "speed": 100
        },
        {
            "id": "533c8b0d-e791-4185-8330-9a6022d8f3b4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 719.287842,
            "y": 1237.24536,
            "speed": 100
        },
        {
            "id": "0ce677cd-fc08-417b-91e8-ad354b8586f2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 745.816162,
            "y": 1248.56616,
            "speed": 100
        },
        {
            "id": "0544660e-dbfe-4e5b-b207-1023a799a174",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 770.2312,
            "y": 1242.90576,
            "speed": 100
        },
        {
            "id": "4bcdac8c-930c-4271-948e-a587d9c29061",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 813.627441,
            "y": 1212.717,
            "speed": 100
        },
        {
            "id": "99e9cd9b-ed80-41fa-a96a-a0b6230c92e1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896.646362,
            "y": 1188.18872,
            "speed": 100
        },
        {
            "id": "1a936d76-ecff-4288-92bb-2bba397e3004",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 923.1747,
            "y": 1220.37744,
            "speed": 100
        },
        {
            "id": "9f154fd6-bd59-4323-a891-ee25a0cb5ed9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 941.9293,
            "y": 1241.01892,
            "speed": 100
        },
        {
            "id": "e6c6f6ec-2790-45cf-ac20-aafbe7362993",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 966.457642,
            "y": 1227.81128,
            "speed": 100
        },
        {
            "id": "1c4996ec-a625-487d-934c-5368539a60d2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 994.7595,
            "y": 1205.16992,
            "speed": 100
        },
        {
            "id": "6b22d9ff-5384-4469-8c37-e6f634bbda97",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1015.62744,
            "y": 1205.16992,
            "speed": 100
        },
        {
            "id": "e0c07c8e-124b-49c7-b7c6-ec471d950946",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1032.49536,
            "y": 1212.717,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}