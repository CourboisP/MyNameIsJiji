{
    "id": "d44bfb80-f462-458d-82ab-7f7533546df3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rosalie_falling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b63bcce8-d70c-41ff-9afc-e69ed11d1f82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44bfb80-f462-458d-82ab-7f7533546df3",
            "compositeImage": {
                "id": "6cefed8a-356e-45c0-8b9a-048ff7d253f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b63bcce8-d70c-41ff-9afc-e69ed11d1f82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db702c18-8a94-483a-828b-bdd9d7939f39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b63bcce8-d70c-41ff-9afc-e69ed11d1f82",
                    "LayerId": "f34506d5-3fe9-45a4-8aa9-66046e32a360"
                }
            ]
        },
        {
            "id": "7676c7fa-9b8e-48c9-be6f-cbd7c672b683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44bfb80-f462-458d-82ab-7f7533546df3",
            "compositeImage": {
                "id": "02e19df8-12a5-4db1-a416-ba0867514816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7676c7fa-9b8e-48c9-be6f-cbd7c672b683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d53ab84e-b25a-4a66-8e90-c9651d9e5915",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7676c7fa-9b8e-48c9-be6f-cbd7c672b683",
                    "LayerId": "f34506d5-3fe9-45a4-8aa9-66046e32a360"
                }
            ]
        },
        {
            "id": "b96d529c-b2cc-40f5-857a-223429642db9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44bfb80-f462-458d-82ab-7f7533546df3",
            "compositeImage": {
                "id": "b3bc3222-4105-435a-ae6e-7b7beb5dcd78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96d529c-b2cc-40f5-857a-223429642db9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eabb9a51-e634-4db7-9d89-f0affa7fb3a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96d529c-b2cc-40f5-857a-223429642db9",
                    "LayerId": "f34506d5-3fe9-45a4-8aa9-66046e32a360"
                }
            ]
        },
        {
            "id": "d7c207ea-750e-4362-a490-567af2033c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44bfb80-f462-458d-82ab-7f7533546df3",
            "compositeImage": {
                "id": "99246b59-c3ef-4b54-8f20-4efbdb6fb517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c207ea-750e-4362-a490-567af2033c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7873e44d-2a1b-4e33-98ba-a535cef61b08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c207ea-750e-4362-a490-567af2033c92",
                    "LayerId": "f34506d5-3fe9-45a4-8aa9-66046e32a360"
                }
            ]
        },
        {
            "id": "7b53736f-1c4a-45ac-b495-639f78d98c14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44bfb80-f462-458d-82ab-7f7533546df3",
            "compositeImage": {
                "id": "08a6fe53-3369-44c3-a4d1-31a50d88232f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b53736f-1c4a-45ac-b495-639f78d98c14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d3d8589-7fdb-459d-9009-9e890ad09d19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b53736f-1c4a-45ac-b495-639f78d98c14",
                    "LayerId": "f34506d5-3fe9-45a4-8aa9-66046e32a360"
                }
            ]
        },
        {
            "id": "298cb8c2-86ee-42e1-b50c-cadba5804d66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44bfb80-f462-458d-82ab-7f7533546df3",
            "compositeImage": {
                "id": "fcea2ebb-916b-43f1-a90c-151a0f02f13e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "298cb8c2-86ee-42e1-b50c-cadba5804d66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a899801-876d-4aa3-997f-78e851daa44a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "298cb8c2-86ee-42e1-b50c-cadba5804d66",
                    "LayerId": "f34506d5-3fe9-45a4-8aa9-66046e32a360"
                }
            ]
        },
        {
            "id": "f0abaf5a-c40a-49f0-9a96-cffe1d7be454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44bfb80-f462-458d-82ab-7f7533546df3",
            "compositeImage": {
                "id": "7d0fc0bb-b6a5-48e0-a744-4c2e45afea1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0abaf5a-c40a-49f0-9a96-cffe1d7be454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "906838e8-8918-4e59-805b-f4c087b61503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0abaf5a-c40a-49f0-9a96-cffe1d7be454",
                    "LayerId": "f34506d5-3fe9-45a4-8aa9-66046e32a360"
                }
            ]
        },
        {
            "id": "9976f400-1828-4dc3-8b84-9fc9c001393c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44bfb80-f462-458d-82ab-7f7533546df3",
            "compositeImage": {
                "id": "ccf75453-f344-4910-922d-e84e9b1c47fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9976f400-1828-4dc3-8b84-9fc9c001393c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a14bbc23-bde1-4270-8f04-687d9883bb35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9976f400-1828-4dc3-8b84-9fc9c001393c",
                    "LayerId": "f34506d5-3fe9-45a4-8aa9-66046e32a360"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f34506d5-3fe9-45a4-8aa9-66046e32a360",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d44bfb80-f462-458d-82ab-7f7533546df3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}