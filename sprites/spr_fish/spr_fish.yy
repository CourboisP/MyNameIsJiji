{
    "id": "041484c4-f8fa-4a95-a335-0593a592110d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fish",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 7,
    "bbox_right": 64,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7273a697-d009-46b4-8edf-fa5b43b3de0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "041484c4-f8fa-4a95-a335-0593a592110d",
            "compositeImage": {
                "id": "1817acb5-62dc-476e-aa94-0cafa5474d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7273a697-d009-46b4-8edf-fa5b43b3de0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8369634-6c44-4a44-9373-0fa1c20e4113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7273a697-d009-46b4-8edf-fa5b43b3de0c",
                    "LayerId": "3c0d00e3-2cd4-4714-a669-93fdd8201540"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3c0d00e3-2cd4-4714-a669-93fdd8201540",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "041484c4-f8fa-4a95-a335-0593a592110d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 35,
    "yorig": 11
}