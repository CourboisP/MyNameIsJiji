{
    "id": "7d3235d4-b67e-4415-a413-e71783853d17",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cloud2",
    "eventList": [
        {
            "id": "f4d51359-cbde-4970-881b-b01d8fdf9f8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d3235d4-b67e-4415-a413-e71783853d17"
        },
        {
            "id": "64f08255-e2f0-4445-830e-e6b2b9c75bd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "7d3235d4-b67e-4415-a413-e71783853d17"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ba8fc4f-da5a-4053-beae-68d865ba7561",
    "visible": true
}