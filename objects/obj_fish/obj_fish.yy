{
    "id": "34959b1b-3aef-4e0a-9612-303b95db5fa5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fish",
    "eventList": [
        {
            "id": "149e3a4b-629e-4e3a-b60b-b90d7ac6553e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34959b1b-3aef-4e0a-9612-303b95db5fa5"
        },
        {
            "id": "feca0111-2dfd-4217-9227-9046cb46f37b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "34959b1b-3aef-4e0a-9612-303b95db5fa5"
        },
        {
            "id": "a46f9cce-811a-4d37-b866-32abc84f447e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "34959b1b-3aef-4e0a-9612-303b95db5fa5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "041484c4-f8fa-4a95-a335-0593a592110d",
    "visible": true
}