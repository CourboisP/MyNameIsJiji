{
    "id": "b5b03d34-4508-41cb-8b86-b851637389cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buttonOn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 10,
    "bbox_right": 53,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2369d68-a186-4a2a-848c-f1c15e24445a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5b03d34-4508-41cb-8b86-b851637389cf",
            "compositeImage": {
                "id": "059ec27e-35a2-4993-95a0-1ddc984813ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2369d68-a186-4a2a-848c-f1c15e24445a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fed916b6-2960-4528-b456-8a0c41586036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2369d68-a186-4a2a-848c-f1c15e24445a",
                    "LayerId": "5b9be543-aa40-49a2-84f9-c5519ddaaab2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5b9be543-aa40-49a2-84f9-c5519ddaaab2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5b03d34-4508-41cb-8b86-b851637389cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}