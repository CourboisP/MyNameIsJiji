{
    "id": "a15bb364-3e78-4917-a587-154360268ff8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fish2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "803daae1-5278-45db-9cd4-1d49999510e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15bb364-3e78-4917-a587-154360268ff8",
            "compositeImage": {
                "id": "2d560347-9796-4ef3-a920-68321ae96bd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "803daae1-5278-45db-9cd4-1d49999510e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90a55b2b-1fdb-443c-90b4-8a3ac3f29076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "803daae1-5278-45db-9cd4-1d49999510e7",
                    "LayerId": "40006fa8-91f1-41a1-969a-43342b6a9c6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "40006fa8-91f1-41a1-969a-43342b6a9c6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a15bb364-3e78-4917-a587-154360268ff8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}