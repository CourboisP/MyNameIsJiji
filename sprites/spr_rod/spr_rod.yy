{
    "id": "5fc239df-1236-4359-ae3e-1394a2e6a8c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rod",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "403b24da-9ca6-4586-964c-e02eea0d1d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fc239df-1236-4359-ae3e-1394a2e6a8c5",
            "compositeImage": {
                "id": "c09b8737-74d0-43d9-b48b-9d97ab996938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "403b24da-9ca6-4586-964c-e02eea0d1d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f01d5271-c496-49ef-b643-ea7c6342c01a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "403b24da-9ca6-4586-964c-e02eea0d1d2e",
                    "LayerId": "ece1fda1-63a6-4b05-944b-803e832b2080"
                }
            ]
        },
        {
            "id": "32cc91a3-c46b-4ef5-b963-30fd62916f4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fc239df-1236-4359-ae3e-1394a2e6a8c5",
            "compositeImage": {
                "id": "9afdec65-c4ff-4d9c-8195-e3675cbfe80c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32cc91a3-c46b-4ef5-b963-30fd62916f4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9998240-5804-4043-934b-b24883dca947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32cc91a3-c46b-4ef5-b963-30fd62916f4c",
                    "LayerId": "ece1fda1-63a6-4b05-944b-803e832b2080"
                }
            ]
        },
        {
            "id": "802369e7-5975-4da7-bc13-bce1166fc92d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fc239df-1236-4359-ae3e-1394a2e6a8c5",
            "compositeImage": {
                "id": "46a871c1-c3e8-4c1a-8d22-0d0a42b6b594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "802369e7-5975-4da7-bc13-bce1166fc92d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a46b5155-8741-452b-bcf9-8829161f6390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "802369e7-5975-4da7-bc13-bce1166fc92d",
                    "LayerId": "ece1fda1-63a6-4b05-944b-803e832b2080"
                }
            ]
        },
        {
            "id": "9683f03d-44e7-4df4-9fec-c5ed9c50fbce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fc239df-1236-4359-ae3e-1394a2e6a8c5",
            "compositeImage": {
                "id": "984ab67a-1868-4964-ad86-90da2f204420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9683f03d-44e7-4df4-9fec-c5ed9c50fbce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aeb5c3e-3488-4cfd-b689-b8e599a195fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9683f03d-44e7-4df4-9fec-c5ed9c50fbce",
                    "LayerId": "ece1fda1-63a6-4b05-944b-803e832b2080"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ece1fda1-63a6-4b05-944b-803e832b2080",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fc239df-1236-4359-ae3e-1394a2e6a8c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}