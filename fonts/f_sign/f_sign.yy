{
    "id": "1e4df168-66d8-4d37-ae19-dc1e7cd059e8",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_sign",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Droid Sans Mono",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4d3ca47f-7842-4aab-84bb-474b6ae4acf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1247bc02-09d7-4672-a5d4-47a10735c99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 173,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "dabeaf60-6e27-4578-9e70-a7b13dad9e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 165,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "25869dfb-b8aa-442a-a9cb-d7f721496fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 153,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6befad18-6746-481a-833f-56be44cf6b1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 143,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "60d86fd6-86be-4b63-a2a1-5dae526c78a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 131,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "42b20b4d-e74a-428a-a136-8e8aa3f5b289",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 119,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c046d823-a44f-4b1d-b4a4-4ec1138f461c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 114,
                "y": 48
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7978b5a8-2d38-4ef3-a9ce-f1d5c674840e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 106,
                "y": 48
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e41c4616-9019-49eb-85a6-3c05b446d708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 98,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6c612df7-437e-4b71-a945-6358303b4b5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 178,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d0695474-0f68-41cc-9bd9-f99c7a56904d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 88,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "219255fc-14ea-48d0-a83b-40c1136ac630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 72,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fb90ccc1-5d44-4eaa-97b1-b2ba2c7e49dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 64,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "12bae70b-5cef-4f4e-82f7-ab6841ab7c10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 59,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "03452695-93d3-42a5-b746-24bd24f49437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 50,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fc2547b6-de65-417f-ad0e-4134c6579b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 40,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "405fed6c-b8b6-46df-99fb-37310e97c40d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 5,
                "x": 33,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "badeec06-6990-49d4-aef6-a26658d73a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 23,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "10594ed8-1864-4c7e-b45e-26beb36b83f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 13,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ec75e0b3-7fc1-4755-8e2f-17dd733e7636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "da3bb7db-2477-411a-8c43-47608ed8f489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 78,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1fa7e42f-fba9-4df3-ac99-27bb2f5bd43a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 188,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b1c1e0c5-8b4e-44b1-87b7-07ec24b43eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 198,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "edbcf006-bb48-4ec3-9d86-eefe21bcd8d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 208,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a68acfb9-6087-42ad-9b7a-9cd1001ee58c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 166,
                "y": 71
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "121d8051-f555-4d8c-be0b-212809ab3d3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 161,
                "y": 71
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cd6205cc-18aa-47f1-aacb-c463c9a2a367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 156,
                "y": 71
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5c35629d-6f87-4a5b-88bf-5c178a28b993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 146,
                "y": 71
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "11b0ea98-f7cc-448d-a78e-d76d16e0617b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 136,
                "y": 71
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "528567a3-76ba-46fd-85c7-6787f46a7f0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 126,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7a1b3c54-1db7-4171-ad8e-1a3c2077fe18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 116,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "272ceadf-7e41-4713-9e4a-32d032030ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 104,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "223ccaf4-ac43-46ac-80e5-0de31a263349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 92,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bdc4221a-1c0f-4d5d-962b-4297cf5ce137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0071f46b-8fd6-4b18-a359-e7c057c10e9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 71,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b289d5b8-9c31-460a-abac-856e6b922370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 61,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "350f9255-62e3-465a-9abf-a49e45103af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 51,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d4759bf6-317a-463b-9e36-50fef3d06da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 41,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6e269055-17a6-47f0-9269-9d8a49333000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 30,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "45a29ac4-738d-401a-b548-2a6663c9dc40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 20,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "21df146a-7be2-463b-9948-67da6452e733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 11,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9f9f80b3-ae91-4852-b4bf-772aaf7dcacf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e4f12061-a2e0-4773-b750-656723b96a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 239,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ef2a35e2-52d2-4a5c-94c7-8a424f92045c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 229,
                "y": 48
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d79a30d4-b6ab-4904-bc8c-c65a9f85d07c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 218,
                "y": 48
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c35c3085-92b2-4813-9235-8549b9ad1f44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 241,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a71e7182-a212-4027-aa5d-0e4a5ac6c5b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 25
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "99249bc8-6b53-40bc-9e5f-abd4c2c6ad9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 220,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "879b1bd5-d69e-410b-823e-1e7d6bf6e9ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "210780a2-2800-47b0-b21b-21875105f9f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "55c4c4f9-d33c-44f3-9e20-31b22c57ecaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9beddcc7-d39f-4461-a71c-c03d7538c2fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "768c0067-a66f-4d6a-8122-8b70408b3831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1c344b00-7cee-4b4e-872f-570757ca8d61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "94bd8377-c068-4737-b594-1022f4781bc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4453f536-560e-44e5-ba54-59fa0849a8cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1fb1970a-39b7-473f-a3c2-2434d1748ab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c97da9a0-4500-41c0-ab65-1a75bd902f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "45f450ab-5086-4d38-bfdc-c50f21e405e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9144c988-5fad-42c1-b4e9-763b3095d67b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "47f38246-3f4a-46f6-bf2d-1a224c3e4e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "afb8e530-9986-459d-b274-f7a40378d412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "28389b74-a7f4-4e85-a2ac-a156b5166ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "39dd811f-9f2f-4dde-891c-8932978d6d5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "84e4e576-8a62-4b36-a3a5-f8a01095fde1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "36fa9da3-ddc7-47ac-b7e7-8b23803a926a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "38caec90-0fd8-45dd-b565-634e7ea67f5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b9cfbcc3-77c8-4fc5-8ae6-238fa506b32c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f644e933-a7de-4099-8926-044eee2b093d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c728954f-b9cc-46f7-a04c-6785ad851e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d8c8af05-19f7-4c13-8679-65ec2ae9b158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0d77c7a9-5c33-43b7-9322-feb452af5db4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 100,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4e86cf65-cdfb-41b2-8c0f-857ea751bb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b86108ab-12bf-47df-875f-b36215da529e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 202,
                "y": 25
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3e9da8a1-3a80-4386-b43a-98dcc7fe078b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 191,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "10dd7f8f-fd63-4c41-abec-72779b769ffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 181,
                "y": 25
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c941a32c-e0ee-46dd-9ec3-8f0544b0dc14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 170,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1c6384f4-4d1a-4587-8a2b-b14a734ce381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 160,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9063cb3c-c106-4fe1-9d90-a743c5a7d874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 149,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "af157c1e-d990-42a4-b6a8-dd83280154e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 139,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f0e072d2-c154-4be3-a96f-3329ea80ed3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f1e147b9-3645-4f4c-b4d6-f7d77b341ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 120,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "057e1685-fac2-4b65-a8a8-a996a0c82976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 210,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b40709b6-eb20-4115-a947-4cc69853a83e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 110,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9a5b5517-a1a4-4e89-9bd9-c0510515d982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 90,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d5943e85-57e1-4fcd-87ef-6795402750f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "be9e7b2f-340e-4d60-a9f3-68e7b50be3b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 66,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "65a0f542-e03e-4aa6-a3e8-81af910b5e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 55,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ca588b40-0dc6-452d-bcd4-76cd8aeccefc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 44,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "656371a4-0ec6-4372-8432-ed44bb862153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1237dc32-8bd7-4ab3-a583-d7afe4351a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 25,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9ca6d688-b34a-430b-943a-43c01a71b769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 21,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b74271ab-e43e-4cb3-9d0a-17b415e1738d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 12,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "30dba9d6-4fbc-40a1-8b9a-8ad1d38e4903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 176,
                "y": 71
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "d5ea50b8-55cb-43ee-97b8-7f7c16125450",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 186,
                "y": 71
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}