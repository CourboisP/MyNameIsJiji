{
    "id": "5ec2cff3-1c1d-4f81-a680-19973fa3ef00",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_last_fade",
    "eventList": [
        {
            "id": "605b01d9-4a32-46b7-84a9-34a7383dda51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ec2cff3-1c1d-4f81-a680-19973fa3ef00"
        },
        {
            "id": "3f7c9ca6-0f0b-45ae-8406-ae9e5c73e5e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5ec2cff3-1c1d-4f81-a680-19973fa3ef00"
        },
        {
            "id": "04808274-1dd4-4b03-b818-d6615c128fe4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ec2cff3-1c1d-4f81-a680-19973fa3ef00"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}