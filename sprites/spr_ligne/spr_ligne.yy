{
    "id": "1225a6ef-9874-432a-92d4-6b7550bfbfbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ligne",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 7,
    "bbox_right": 8,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3340fe67-5ae8-4ba6-bd3b-0fdfef562244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1225a6ef-9874-432a-92d4-6b7550bfbfbe",
            "compositeImage": {
                "id": "868d4afe-40f5-44f4-8418-8aba6f07ba85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3340fe67-5ae8-4ba6-bd3b-0fdfef562244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f495067-c1ed-47c5-a58a-f4721faf6df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3340fe67-5ae8-4ba6-bd3b-0fdfef562244",
                    "LayerId": "78c5f967-4ef5-4639-b87d-e60f7ba5565c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "78c5f967-4ef5-4639-b87d-e60f7ba5565c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1225a6ef-9874-432a-92d4-6b7550bfbfbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}