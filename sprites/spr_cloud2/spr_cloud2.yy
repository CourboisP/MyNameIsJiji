{
    "id": "6ba8fc4f-da5a-4053-beae-68d865ba7561",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloud2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2807cac2-da3d-4131-b9f8-571098f29d4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba8fc4f-da5a-4053-beae-68d865ba7561",
            "compositeImage": {
                "id": "e0ce7937-491a-4f1c-98d9-a0bfac4de63f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2807cac2-da3d-4131-b9f8-571098f29d4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7462ac26-2673-43f7-9fd0-e586bfc299f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2807cac2-da3d-4131-b9f8-571098f29d4a",
                    "LayerId": "8011b096-0b00-4546-a990-ebedca7f24a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8011b096-0b00-4546-a990-ebedca7f24a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ba8fc4f-da5a-4053-beae-68d865ba7561",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}