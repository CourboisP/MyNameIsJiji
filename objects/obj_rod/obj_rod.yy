{
    "id": "02c7da35-f373-416f-8971-42ff41fb81da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rod",
    "eventList": [
        {
            "id": "2138b558-f5b5-4af7-aa19-e8c73de8e35b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "02c7da35-f373-416f-8971-42ff41fb81da"
        },
        {
            "id": "8da68a80-70fb-4df9-8aa7-b8071e07a9ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02c7da35-f373-416f-8971-42ff41fb81da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5fc239df-1236-4359-ae3e-1394a2e6a8c5",
    "visible": true
}