{
    "id": "6ccf1f71-bc2c-4a26-aaa0-be66eceb930e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_seagull2",
    "eventList": [
        {
            "id": "a11bdf2e-4260-4a93-ad75-0b44683d380a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ccf1f71-bc2c-4a26-aaa0-be66eceb930e"
        },
        {
            "id": "bcc1f461-007e-4414-9c58-42f1b1c11d03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "6ccf1f71-bc2c-4a26-aaa0-be66eceb930e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
    "visible": true
}