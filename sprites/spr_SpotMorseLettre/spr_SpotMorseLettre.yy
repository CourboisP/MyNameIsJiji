{
    "id": "3e48e583-b09d-49cf-a778-ce4fc538cb48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SpotMorseLettre",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "735cf3dc-24f8-4e3d-9fa5-6d94a92e43da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e48e583-b09d-49cf-a778-ce4fc538cb48",
            "compositeImage": {
                "id": "18caaf93-8a03-4871-9724-ab60d04ada62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "735cf3dc-24f8-4e3d-9fa5-6d94a92e43da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d74d135-cc2f-4365-bec6-58661e4d61bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "735cf3dc-24f8-4e3d-9fa5-6d94a92e43da",
                    "LayerId": "9682ba21-b149-4c9f-8a29-ce8f231efd37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9682ba21-b149-4c9f-8a29-ce8f231efd37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e48e583-b09d-49cf-a778-ce4fc538cb48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}