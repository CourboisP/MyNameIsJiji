{
    "id": "90e80f97-260d-4bb8-8142-a6bd092ea994",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bait",
    "eventList": [
        {
            "id": "08687b78-04ac-4dbe-89b9-4e4a72d28556",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "90e80f97-260d-4bb8-8142-a6bd092ea994"
        },
        {
            "id": "d376060d-16e2-4049-a949-3bf089e5d1b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90e80f97-260d-4bb8-8142-a6bd092ea994"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7d8f6449-dd07-4cbe-a2a0-da0f358fec07",
    "visible": true
}