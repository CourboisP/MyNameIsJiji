{
    "id": "e198e559-5767-47f3-aa47-7d173cbd1519",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wave1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "760b6f82-d11f-4fa6-99a1-47b459a7b0d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "9b8680dd-f932-4dbc-91f5-0d4fe4e86f35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760b6f82-d11f-4fa6-99a1-47b459a7b0d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d2d7035-62e4-412e-a0ee-438a14132f23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760b6f82-d11f-4fa6-99a1-47b459a7b0d7",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "4b98c0fd-24ce-444c-88a7-35770e6a786e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "0b08cab7-00b6-45e7-ba44-250a6f376da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b98c0fd-24ce-444c-88a7-35770e6a786e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9413133b-6713-4bbb-bd38-79c97aa33dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b98c0fd-24ce-444c-88a7-35770e6a786e",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "210494a4-ab5f-433a-9fb7-7c3c561b6782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "e18e24f8-69c0-4f4b-8bb1-2419d5f0a1a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "210494a4-ab5f-433a-9fb7-7c3c561b6782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cbbe0bc-cdb5-4f64-8855-ed167604b1ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "210494a4-ab5f-433a-9fb7-7c3c561b6782",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "61cf4f73-3f41-42ae-886e-a48ce7007f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "58fe3e65-d0d5-466c-ae70-a0ac50299161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61cf4f73-3f41-42ae-886e-a48ce7007f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f104ef5-3bc1-4c46-8ed1-a7ccb95569d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61cf4f73-3f41-42ae-886e-a48ce7007f16",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "b5d76e8a-a73a-4a7c-b42a-7732e6e03097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "92f7cf4b-753d-4bae-8b0a-dc6c165218f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5d76e8a-a73a-4a7c-b42a-7732e6e03097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcbf4eb7-809a-41c5-9c79-96cdff091ee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5d76e8a-a73a-4a7c-b42a-7732e6e03097",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "8b99a402-bce9-4cba-81dd-82430f1cddae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "bb8ad672-b58f-4186-9141-dc5a5bcacd82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b99a402-bce9-4cba-81dd-82430f1cddae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61483e0e-5dfc-411a-939b-0295a1b464d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b99a402-bce9-4cba-81dd-82430f1cddae",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "913793d1-5f36-41bb-a830-42ec541c7d07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "c2fcb810-517f-4ff0-ae0a-771b46728ac0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "913793d1-5f36-41bb-a830-42ec541c7d07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f2d44cd-9817-4ac8-b065-352ca0b5be39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "913793d1-5f36-41bb-a830-42ec541c7d07",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "1ef719c4-6dac-4ea4-bf5c-21be34c060a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "dd1d5fe9-c26a-41f6-9996-189d88509833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ef719c4-6dac-4ea4-bf5c-21be34c060a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "797ec2a1-96b2-471a-83d0-a99b9439807d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ef719c4-6dac-4ea4-bf5c-21be34c060a7",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "28678fc1-52d4-444f-ba34-952275d5f462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "e8f849cd-03d6-484f-8e4a-9cc8002b16bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28678fc1-52d4-444f-ba34-952275d5f462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e030d53-4dc7-48a7-a7b5-de1ceb0c4fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28678fc1-52d4-444f-ba34-952275d5f462",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "b9117be1-db31-4b0b-9a75-e98eeeeeabbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "74f01112-26af-4913-85a1-087d0b588033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9117be1-db31-4b0b-9a75-e98eeeeeabbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "967629c2-550a-440e-bef0-6dcf1c62fd21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9117be1-db31-4b0b-9a75-e98eeeeeabbe",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "7f40b5e6-c61c-432d-9251-526215b274c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "e707bb09-08a3-494f-a249-7b7d49fbf600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f40b5e6-c61c-432d-9251-526215b274c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d232619-3981-471b-acc1-600bb2bd230a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f40b5e6-c61c-432d-9251-526215b274c5",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        },
        {
            "id": "e42804d0-52c5-4096-be03-d37c186bc156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "compositeImage": {
                "id": "3f87c78e-af3b-4bcc-b4c7-4aa578d5d1f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e42804d0-52c5-4096-be03-d37c186bc156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b34fe61-2ba5-4cb1-ae43-99a1e16a4b74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e42804d0-52c5-4096-be03-d37c186bc156",
                    "LayerId": "cdcce38f-afef-486f-934f-ab8c4d232df5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cdcce38f-afef-486f-934f-ab8c4d232df5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e198e559-5767-47f3-aa47-7d173cbd1519",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}