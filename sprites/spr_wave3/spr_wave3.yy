{
    "id": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wave3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f68ccc1-e7d6-4721-969e-0127b68c2a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "50645fe5-e87d-4e0c-9e46-44620ebd7c31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f68ccc1-e7d6-4721-969e-0127b68c2a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fc3d3ab-2fb1-4e44-adf5-95b361a3d6cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f68ccc1-e7d6-4721-969e-0127b68c2a2d",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "81476d8a-f8e6-4478-8588-0375809a54c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "5cdf9520-33fc-4a78-a0cd-89db59c75949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81476d8a-f8e6-4478-8588-0375809a54c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "168918b7-bdd9-4fb8-b585-880ee003be86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81476d8a-f8e6-4478-8588-0375809a54c7",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "1170b331-8acc-4fa0-8fcc-86a221ad2a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "b09ce0e3-8aee-4c78-aac3-2f8db5fe529b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1170b331-8acc-4fa0-8fcc-86a221ad2a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ba3bbe7-635b-4aa2-8350-2ae8c0dae2fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1170b331-8acc-4fa0-8fcc-86a221ad2a96",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "19b8d552-b7db-4baf-bbb0-1d9d5cf71cfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "7f32cd02-851f-4877-8232-c3857486e745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b8d552-b7db-4baf-bbb0-1d9d5cf71cfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b88dcd6-2151-469d-9152-785f31702ca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b8d552-b7db-4baf-bbb0-1d9d5cf71cfc",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "0f3041df-529f-434a-8efb-190f699e4b3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "fa72ecb8-b682-4e06-b4c7-8220fb0d734a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f3041df-529f-434a-8efb-190f699e4b3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5663b05-f121-4cba-a158-8e3b7396748b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f3041df-529f-434a-8efb-190f699e4b3f",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "fa165262-570c-4fbf-9b74-452c6a225e88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "dbf42ed7-7464-42aa-a893-5dc45641a5f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa165262-570c-4fbf-9b74-452c6a225e88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca14fbd2-f190-423e-bfa4-602c34e25373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa165262-570c-4fbf-9b74-452c6a225e88",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "f055ce17-6d64-4a07-9f70-37808d54b811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "8a12a173-2b8e-418e-9ba9-4dabf74fb9f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f055ce17-6d64-4a07-9f70-37808d54b811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9614795-aab3-4f3c-a452-ad9e3e132f92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f055ce17-6d64-4a07-9f70-37808d54b811",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "ccf27d26-9683-4a4a-b82e-ec32f06d195a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "2187d710-7e97-498c-b524-0d8e05f22e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf27d26-9683-4a4a-b82e-ec32f06d195a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "421409ad-8737-4a3a-bed3-d84240e9b009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf27d26-9683-4a4a-b82e-ec32f06d195a",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "eef097bc-6809-4bc6-a6cf-ca48db7baa20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "4b213229-bd05-467a-a5cd-00deac0585c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef097bc-6809-4bc6-a6cf-ca48db7baa20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13eea06c-ba88-48fb-a699-ee8095f4fd02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef097bc-6809-4bc6-a6cf-ca48db7baa20",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "022b189a-016c-4af8-b9e7-8a3f99f9574c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "3cf13e15-7dd1-4ebe-8a84-dfc17583b827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "022b189a-016c-4af8-b9e7-8a3f99f9574c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98ea1ed4-ee3e-41ac-b5f7-39364822b9b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "022b189a-016c-4af8-b9e7-8a3f99f9574c",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "68c9cebd-c13e-4ddc-8ccd-a323f4cb11a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "fc01d835-3b75-46fa-abc0-383b03b0730b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68c9cebd-c13e-4ddc-8ccd-a323f4cb11a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "517c6817-b049-4d7f-982e-b1b00e2e5d9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68c9cebd-c13e-4ddc-8ccd-a323f4cb11a7",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        },
        {
            "id": "2774a04e-4181-4488-86f5-a7232cfb949f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "compositeImage": {
                "id": "8eae69e2-b590-45f3-b5bd-5d1ae499a1df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2774a04e-4181-4488-86f5-a7232cfb949f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1237675-5803-470a-8b3c-276563d028be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2774a04e-4181-4488-86f5-a7232cfb949f",
                    "LayerId": "2e7dba11-851e-4948-9961-bcf78a69c693"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2e7dba11-851e-4948-9961-bcf78a69c693",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ba23dea-07f4-49ab-b632-ac7f40eda6b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}