{
    "id": "03dc5be0-5f9a-4947-8e2c-ca61df483986",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_seagull",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 25,
    "bbox_right": 69,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0e7b4c0-b4c8-4bc0-b063-044e1ea67dd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03dc5be0-5f9a-4947-8e2c-ca61df483986",
            "compositeImage": {
                "id": "c8ce3cd9-e8da-4c10-bdfb-fcb86e8033f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0e7b4c0-b4c8-4bc0-b063-044e1ea67dd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df261ba7-194a-4b0e-8995-185d34552d4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0e7b4c0-b4c8-4bc0-b063-044e1ea67dd3",
                    "LayerId": "d589848f-0d05-4e82-9bed-1ef8d0941127"
                }
            ]
        },
        {
            "id": "85b3dfe0-cb4e-4e16-8ab2-879d3529bc35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03dc5be0-5f9a-4947-8e2c-ca61df483986",
            "compositeImage": {
                "id": "86730338-6a84-4fe3-8640-d79ea77abb54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b3dfe0-cb4e-4e16-8ab2-879d3529bc35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b20539da-1ebf-4f9a-aa6b-60303daabc45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b3dfe0-cb4e-4e16-8ab2-879d3529bc35",
                    "LayerId": "d589848f-0d05-4e82-9bed-1ef8d0941127"
                }
            ]
        },
        {
            "id": "035d15dc-0712-4fb8-8a5b-780389dc5131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03dc5be0-5f9a-4947-8e2c-ca61df483986",
            "compositeImage": {
                "id": "64cedd0d-f71f-4c26-b1df-9cd54cc296fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "035d15dc-0712-4fb8-8a5b-780389dc5131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5395f101-efce-4303-ac66-169c5ed3cb88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "035d15dc-0712-4fb8-8a5b-780389dc5131",
                    "LayerId": "d589848f-0d05-4e82-9bed-1ef8d0941127"
                }
            ]
        },
        {
            "id": "ff5e48ba-9f61-4095-bd13-8b3f4603bfe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03dc5be0-5f9a-4947-8e2c-ca61df483986",
            "compositeImage": {
                "id": "6126c964-8b04-4d76-b8dc-2dc417e54d8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff5e48ba-9f61-4095-bd13-8b3f4603bfe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "156fb3b5-d32a-49d2-9571-36e0d68484dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff5e48ba-9f61-4095-bd13-8b3f4603bfe5",
                    "LayerId": "d589848f-0d05-4e82-9bed-1ef8d0941127"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "d589848f-0d05-4e82-9bed-1ef8d0941127",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03dc5be0-5f9a-4947-8e2c-ca61df483986",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}