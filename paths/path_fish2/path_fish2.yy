{
    "id": "5172f6af-1283-4de2-9dac-3597cfbf48b4",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_fish2",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "f3799536-b47c-47ff-b4fe-263d6cc07e47",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 832,
            "speed": 100
        },
        {
            "id": "9c3740ed-396a-43e4-8a09-5e5e7d49fa86",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 864,
            "speed": 100
        },
        {
            "id": "7df09a6e-8fa3-49b4-8579-e95ecbfff548",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 832,
            "speed": 100
        },
        {
            "id": "d4bb105e-9422-46c1-939e-ad97391ff4b3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1056,
            "y": 800,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}