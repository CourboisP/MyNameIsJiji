{
    "id": "1e4df168-66d8-4d37-ae19-dc1e7cd059e8",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_sign",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Juice ITC",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f9cfd92d-7ab7-48a0-8dcc-78fb82816368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "292d9267-e255-4af7-a2d4-75583ec160cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 119,
                "y": 56
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "58aa6884-a023-42b8-8ed3-e6466241ae86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 111,
                "y": 56
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "18ef99d3-8b9b-470c-a86e-f6e9500aa3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 99,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "526b2b1e-28d6-4077-b154-0be08b847d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5fa5a018-5fd3-46dd-be95-9d178520af2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 76,
                "y": 56
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "250dfdcf-16fb-488b-8786-dc15f007818d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 65,
                "y": 56
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "236cf1a8-afbe-4703-b41f-9d56aa2e7ec4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 59,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4a240a68-bbc1-43a3-af50-4b4d099d40d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 52,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "81e5ed24-3dd5-4aa2-959c-3e2b51a0533a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 45,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "715fe8b2-7632-477d-9352-f5075758f224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 126,
                "y": 56
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "184c2fd0-505c-46ca-9a55-5c0d647bfd9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 29,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c68cc078-15e6-4ed8-973f-417216f494b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 15,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "cf5daff5-263a-42f0-a7c4-37dc4425c50b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 7,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "450c946d-53e3-4c50-b777-8496df5e6f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6daf7cbd-fa58-476e-89d7-6a3f0ddb1092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": -2,
                "shift": 6,
                "w": 8,
                "x": 244,
                "y": 29
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "36be3615-e194-4f5f-83c9-3511f29fe07e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 235,
                "y": 29
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8440e5dd-8ff7-4f73-8aaf-6313786b51d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 228,
                "y": 29
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "45db4d5e-cbfc-4c4e-a44a-2f0bd19f666f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 219,
                "y": 29
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d76c248e-6ce8-4611-9ecf-c9a04e845770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 210,
                "y": 29
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9897c9d8-26be-4e97-808e-2b00e37242f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 201,
                "y": 29
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9d4991c5-0f71-4714-91c0-899c8fdb7ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a9352868-442f-4234-8602-abf122793c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 134,
                "y": 56
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "410b8dc3-4197-4c2d-939d-3d39f254b94a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 144,
                "y": 56
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4a5ecbfc-3a39-4b11-9492-23623a3862d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 153,
                "y": 56
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6f44d36b-bcb8-4b59-9315-c529d0f8984d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 119,
                "y": 83
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "dd08ea5e-8813-446c-a2c1-a39aad0444f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 114,
                "y": 83
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "bcd0b323-e0ec-429f-a02b-79023fd3dd94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 109,
                "y": 83
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f8b408ed-dab7-4618-80b6-3eb2445fc74a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 97,
                "y": 83
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "38779a57-613d-4421-8144-f651c3788cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 81,
                "y": 83
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c31d3f5c-cfd9-4bfb-ac68-0d8f100abcd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 69,
                "y": 83
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "35e95af3-64df-4635-95a9-77ea6524f986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 60,
                "y": 83
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1600b937-2197-47a3-a1c9-091179ec0455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 40,
                "y": 83
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3ee7c6e5-20ad-41e0-89b9-a6dbfc23855f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 29,
                "y": 83
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b3039dc5-d9c3-4a05-8012-a8484de6783a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 83
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e9bc7c5e-76ad-466c-a7d5-a79b19fd7988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 83
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "28b8ac4d-b5e8-4378-8216-6f3c82b68e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2f2ea72b-3eb8-4541-8d7d-f7d6416386d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 238,
                "y": 56
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ed816319-5313-4644-8a8c-5cac30ac69e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 229,
                "y": 56
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f407b011-8452-458d-b5d9-782f26327aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 220,
                "y": 56
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5d31965a-281c-4e09-9cf5-9bb89b377f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 210,
                "y": 56
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "26a40c95-2042-4f27-be96-88af940ab61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 202,
                "y": 56
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "43cdc602-bd5d-4963-8f2c-8c58fd6879b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 193,
                "y": 56
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "021f2f4b-9af7-47a2-a477-6ccdb756f9a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 183,
                "y": 56
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a2cc44a0-d83f-490b-b18a-d2ff8002d63e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 174,
                "y": 56
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "53d5be09-ea6f-4fd6-ab1c-27987dba641a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 162,
                "y": 56
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "073ac956-82c8-43a5-b084-a8cae5dcd17a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 29
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ada3b546-a1c7-4133-ad7f-cb23571df3c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 182,
                "y": 29
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "656bfe62-ee6e-43cb-a3e5-7f85cf20ec40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 173,
                "y": 29
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0aac21a1-4c5e-44d5-8d77-a6f48f265e17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f391c90b-ddec-4ba2-9891-a4136c5ad2c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d97f7125-8537-4d22-8c6d-ea7052fce94f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8b465940-d895-40cf-b1f8-5c037824e8f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ba55e158-52ac-4a97-8c6e-23a34f0b6a9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "395435c0-b5c5-46d7-8858-af52d9026630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5caa0751-b2d0-4c13-90dc-a87331560e4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4a583bf9-9c9b-4932-8853-e5468ece6e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "91f45469-72b9-4387-b041-37506d7ae469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9d1137fc-39ab-4122-a1bb-9c279ff3c0d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0b3f250f-c27c-4435-87cf-2f659bbef94b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d156345b-1f42-41d6-9932-1c1aeba9b05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d9864253-a8b5-4677-bed3-8ae783ca64d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "72f5d51f-b914-4c43-b1c6-0b1ba343ab5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "84373990-148b-4e61-b499-c85422037fe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "42b7607f-01d6-4fa3-bbfd-9a74887636db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 3,
                "shift": 11,
                "w": 3,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d63cf221-08a6-4650-8695-95d1ac01e91c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b0b57e6d-271f-4081-937b-1f021ca2effd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "56276e10-e0aa-46ed-b0a6-aee5aa9d24b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "47e376b1-9bc5-4abc-954c-c867085a4031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "80fa9b24-8f11-40c8-901b-fe40d242b59f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1c9ec082-a11c-40d5-b92b-a2937f7099f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "670bef48-9dc3-4db0-b08e-14b008d67b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "dfbdf049-a954-4c6f-b877-da41b6f5b14f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 29
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c21b0141-aaab-4f93-b0d0-3b11d934788c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9832d810-0174-42cf-aa45-8713b59acd12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -2,
                "shift": 5,
                "w": 7,
                "x": 156,
                "y": 29
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8ad5d7ed-48b6-4c38-b67f-5aaa0bbe7816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 146,
                "y": 29
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "894a210f-04ea-47a8-9132-20555041b725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 140,
                "y": 29
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "059b1a7f-a12b-489c-9d80-fe11908fd74b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 127,
                "y": 29
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "160cdcfa-e8b4-44ad-bc1c-75fb0f473563",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 118,
                "y": 29
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d3c58aea-aa43-4bc8-a88a-ad846ad4adea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 110,
                "y": 29
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cc852336-e531-4e2c-b075-3836f2c0b7e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "780a08b4-0826-4733-b5bd-51571c3fd773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 91,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "133fec3d-fc94-4d6f-89a0-bf59864384ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 83,
                "y": 29
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "39af6834-01f7-4f2f-8aa2-956de46b7ed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 165,
                "y": 29
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "425f9918-5007-4f8f-aeea-671fe7c8be91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 74,
                "y": 29
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1c035139-3b90-492d-ae92-1e14958a343d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 55,
                "y": 29
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4c13a0f1-65fe-4fb7-9094-d3ebc0af8c90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 45,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fcb6a509-9816-46df-b7b1-5bbc0178d18e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 31,
                "y": 29
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bb6834d8-9bb5-4d7c-bde4-29e694834190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 21,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "be2fd474-0ba1-4630-bf01-814c53ff402f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 11,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0e65acd0-ec62-44ba-aadf-b4a93582291a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3420c512-c9ee-4423-b705-6515ed954ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "693c811f-6584-4d84-a4d0-776eda57fdd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 5,
                "shift": 12,
                "w": 2,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b6cbb03b-34b5-4eae-baf6-c088b8481eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0828bc5e-8c32-4fd6-9313-ac1498e3b6a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 129,
                "y": 83
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "cbb5ec59-1ffa-4358-adb3-a87b00726b09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 25,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 143,
                "y": 83
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "41d17ee1-e6a0-4be5-b1c4-ead479dd0552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8217
        },
        {
            "id": "afe49abe-5683-42b3-9cc9-ad0a47601bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8221
        },
        {
            "id": "125ea3de-b842-4200-987e-eda189f4f853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8217
        },
        {
            "id": "989fbbed-ee11-4ea7-bb58-6d91c69afc17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8221
        },
        {
            "id": "aa1f24be-2910-4293-a749-2fe042af642d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 44
        },
        {
            "id": "cc84e5bd-1ac9-4c31-9cef-a6eddf42d94e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 46
        },
        {
            "id": "d774b750-f8a0-48dd-9643-0306675bea78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "94c4e714-7e91-417b-9814-0847f4efdce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "0aa134fc-4610-43f4-a373-e45486a52d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "df530a94-9fa1-4beb-99c7-d6064c3c0b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "dcbda65f-610b-43e7-91f4-8851babeb466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "04df8012-f95e-4c50-b388-e5a455b989f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 87
        },
        {
            "id": "ece86d72-6e6a-4dec-be0f-e4314ffef96e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 116
        },
        {
            "id": "f37cd766-5b3b-4e78-a962-5f0cfab49c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 171
        },
        {
            "id": "a616f807-cf88-47ce-adca-8dcda7f92dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 199
        },
        {
            "id": "188dcd95-8722-4cf0-8aed-7cad38f361bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "3c75e5a1-b512-477a-b195-52602852bbf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "0a311f38-2794-4633-a4df-c094633ec5f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8249
        },
        {
            "id": "9e476fb2-80dc-4bb2-b47c-91775a413664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "42033569-536f-45de-b5fe-8a20e859ef55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "5d1ad89b-12a7-407d-9293-c7996c8baa21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "75022318-0a5d-49b8-b5a0-23c08498008f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "a2f8a6cb-264d-46df-aa33-0e23176e770a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "894d70a3-5820-4900-ba48-9081e45f64c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "7188c93d-5ac9-479f-b21b-ab3a4c0db852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "45710960-f317-4591-909f-2d744a1f198f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "f5f88d47-09f4-4413-8b43-5026ba523d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "beb8dfc3-7458-44fb-8d30-250752d8ef8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 65
        },
        {
            "id": "1ccb1a74-2ab0-464e-bc8b-72656a4984b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 197
        },
        {
            "id": "0de873a7-3e39-4352-80d3-378523544fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "6d7b585b-7743-4a4d-8940-dd98e29a0db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "4135cf70-ec3b-443e-a2b5-3859038202c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "d3954969-6308-4426-9199-70f4e40acd66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 198
        },
        {
            "id": "1607be69-9bcd-4967-b9df-33696b327ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "5bd32ae2-0e95-4aed-99b2-68ad776a44f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "11b76e42-f030-431a-bdae-25b0e39f9889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 198
        },
        {
            "id": "1db76f29-2054-4bc9-b296-8402cf19c91d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "179d933f-e2a7-40e6-b6e8-6e44fe38d956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "b3e6ea21-7e32-4a12-9227-de22160586a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "e8f9f520-6f25-4248-bc4a-0ab9c4c24391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 197
        },
        {
            "id": "456ea63b-3350-4696-86dd-1d911ca8254c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "197a5d6e-088a-4987-9771-cecce01c7eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "ffaf309b-9d0b-4981-843c-58744a2e30b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "6ea7db95-5673-400d-90dc-903d7b62233f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "214f563b-d2f5-4379-9f28-e780da76e71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 198
        },
        {
            "id": "1da57f1c-917d-499e-88cd-f781bc55892e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 118
        },
        {
            "id": "3b0a6772-26bb-4097-9948-f57ad97ee5ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "393db9ad-7d09-4c26-97a2-37da664af9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8249
        },
        {
            "id": "0efefdaf-1b2b-4915-ab37-620801e28c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "3fe5eab0-a9a7-4a82-ae91-dea94169072e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 197
        },
        {
            "id": "bc0e5eee-2301-4d11-8c00-257585c6532a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 198
        },
        {
            "id": "ba81ef6f-8fd7-49e1-9b43-17fae3f261a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "e99054b6-dd82-425a-b615-bfa5ded3ecd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "ef099f1d-f750-4900-83b8-15d56cdfcece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "06121272-bdf6-4f47-a260-f6b7f115c21e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "c578f163-c21e-4c0d-9bca-96b496528511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "2fe230b7-bc39-4f05-86e3-9f21165a707d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "914d6cba-bf33-4f93-ab1f-c775c7fcc825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "b501fae6-56a6-4162-89ce-40312b64ec88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "10b47cf9-e16a-4774-87f3-3ba38c9faeb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "b03fa8d3-3671-4b98-b0cf-5ba2379b90be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "2cd708c0-097e-4ef7-837a-beb23004f4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 198
        },
        {
            "id": "18225c29-ec15-44cd-b737-2391869ad2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "7ee56020-160d-433e-86e8-b053fb6cb34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "ef7f9299-a6b7-41dd-89fa-b4ac7ca926f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8249
        },
        {
            "id": "595be49d-1df6-4cd0-94cc-b21484d796a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 58
        },
        {
            "id": "465b0b12-51d1-4dc8-8dc2-9abcd7b2c284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 59
        },
        {
            "id": "84301aae-03f3-4718-b450-500352599c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 198
        },
        {
            "id": "d738d87e-09ef-4c61-801f-cd336b5c322d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 84
        },
        {
            "id": "5447ccee-267e-423d-bb45-8407f7cc7ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "01124c19-3847-4916-b92c-e87d024aed5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "a8b2eb48-2d9b-43bc-bcee-214508798a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "01318506-98ca-44e6-b94d-746b0bb122e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "57b3a137-79a4-4f15-833e-d16f1bfbac02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "2d8a3571-65b9-49bf-b759-f518c5fd7698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 171
        },
        {
            "id": "8a4a57f5-8876-424c-9f20-caea0b69ae8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "86cb28de-42b8-4a96-9e7f-7bdd729cca3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "027fd01a-26fd-4824-9a2a-682f847a8c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8249
        },
        {
            "id": "7ee7084a-c0fa-4f70-b038-0c09e769e687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 118
        },
        {
            "id": "246ed15d-7d92-4cf9-a53f-329d2ed3ad94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 105
        },
        {
            "id": "849d6173-a830-4bcc-8af8-bbb241107657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 108
        },
        {
            "id": "dbd5fbbe-9966-4ed0-8b35-b83b4dcf1374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "fbae9033-54af-41bd-ad8a-1051bc94cdb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "e27fc057-f10b-489e-9238-9dfceb55464d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 58
        },
        {
            "id": "7f4c544d-fcbb-4f39-98f7-3038ea9e299d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 59
        },
        {
            "id": "c3a7a05d-0e63-4524-93a2-4e4467669381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 58
        },
        {
            "id": "7632ba4a-d3e1-4641-af33-42f6b1b8404c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 59
        },
        {
            "id": "cec95d3f-50cc-4c3e-9117-f4f9aef06b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "6362dd1d-0c65-4423-ae20-ddd5123cfa13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "55b8f1d0-93b4-4c37-b840-ea11a5d0d146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "33a98eb0-4ba0-4d32-957d-b476816162c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}