{
    "id": "69577ee1-e9d9-420a-ab6a-24e6027cec4d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fadeNext21",
    "eventList": [
        {
            "id": "3e64160a-5895-4a98-8e4a-dcc64e362141",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69577ee1-e9d9-420a-ab6a-24e6027cec4d"
        },
        {
            "id": "4589cfaf-d22b-4431-a2f1-12e13aef5ce2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "69577ee1-e9d9-420a-ab6a-24e6027cec4d"
        },
        {
            "id": "2e91a237-fb5a-4852-99f9-177f00386d53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "69577ee1-e9d9-420a-ab6a-24e6027cec4d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}