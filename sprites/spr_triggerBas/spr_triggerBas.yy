{
    "id": "d88a3001-fffb-4d73-856f-084bdf5343bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_triggerBas",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e548218-c164-4113-9ce3-f85b66617ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d88a3001-fffb-4d73-856f-084bdf5343bd",
            "compositeImage": {
                "id": "1e83ed2f-d7e6-4163-b304-5196cafab747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e548218-c164-4113-9ce3-f85b66617ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fffdf4cc-dfa7-40c7-ab9d-6ab5bf364ecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e548218-c164-4113-9ce3-f85b66617ee8",
                    "LayerId": "98a85b7a-0067-405e-8e15-dfd9b3e0d943"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "98a85b7a-0067-405e-8e15-dfd9b3e0d943",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d88a3001-fffb-4d73-856f-084bdf5343bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}