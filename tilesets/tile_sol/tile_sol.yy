{
    "id": "0d6a1e31-73aa-422b-a6be-d264e03b27e8",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tile_sol",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 1,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "dc9b0b2a-0477-490e-b944-5eafdbb000fd",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 2,
    "tileheight": 128,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 128,
    "tilexoff": 0,
    "tileyoff": 0
}