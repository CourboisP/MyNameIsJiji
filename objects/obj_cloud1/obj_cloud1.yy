{
    "id": "42c783c2-dfd7-46d3-9bd3-338a52426ddd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cloud1",
    "eventList": [
        {
            "id": "e9dd1d4a-06d0-4ee8-9983-5d0b9dfa6914",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42c783c2-dfd7-46d3-9bd3-338a52426ddd"
        },
        {
            "id": "e630f0a8-706f-4319-b071-fd0bcaeea0f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "42c783c2-dfd7-46d3-9bd3-338a52426ddd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "564c60ff-49d6-46d8-9bb3-5fcf1a39fd30",
    "visible": true
}