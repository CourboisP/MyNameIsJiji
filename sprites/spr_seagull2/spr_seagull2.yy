{
    "id": "97902785-dcf2-44eb-babc-8f1247621de3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_seagull2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 28,
    "bbox_right": 95,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b828f91-1786-434f-a86a-c2d9a18a25f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "93ca3915-8246-454d-88e0-5d3891c28fc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b828f91-1786-434f-a86a-c2d9a18a25f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c25af4e5-78b4-47e1-8a1f-27840ec4c163",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b828f91-1786-434f-a86a-c2d9a18a25f6",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "597026c8-b1c1-4ccc-b4ca-26d10708ce21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "a84b0b24-2f71-4b06-9fb6-a015cc052623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "597026c8-b1c1-4ccc-b4ca-26d10708ce21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa63efa5-f1ab-4ba6-8d02-ac15bc6cc250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "597026c8-b1c1-4ccc-b4ca-26d10708ce21",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "8d771efe-94fb-41f9-b2cf-cb6e5e45b988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "9b1dc12a-d8cc-4e0c-b47a-7a1935ede53f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d771efe-94fb-41f9-b2cf-cb6e5e45b988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9541824f-1a17-4f1d-90b5-f2095a420006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d771efe-94fb-41f9-b2cf-cb6e5e45b988",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "65ad38f7-4cc4-4f33-9980-1af9454c6896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "829d5af0-f559-4bd6-a28f-67180d3d974e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65ad38f7-4cc4-4f33-9980-1af9454c6896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c527190-ab17-4518-9d54-b4d8a34937f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ad38f7-4cc4-4f33-9980-1af9454c6896",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "1cecc3cc-839e-4c5e-abb9-7cc0cd8e7c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "96c98bf5-0c32-4e45-84e6-0ce6cef86b6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cecc3cc-839e-4c5e-abb9-7cc0cd8e7c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76035c64-eab3-4667-8024-f7aa64a409c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cecc3cc-839e-4c5e-abb9-7cc0cd8e7c6e",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "8c290f83-684a-49c1-9ebf-257478e0905e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "73203d0a-3571-453d-b1eb-2b11b0458c08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c290f83-684a-49c1-9ebf-257478e0905e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0770b9-1e02-4d39-971f-b1b72ce86b4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c290f83-684a-49c1-9ebf-257478e0905e",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "a964aa74-9cd9-444b-a418-22fa435a244c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "3c3feb2b-d16c-4726-8114-3982858fcc6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a964aa74-9cd9-444b-a418-22fa435a244c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c9e65f6-338d-4220-8389-a1ddf935de74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a964aa74-9cd9-444b-a418-22fa435a244c",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "67607f5e-b1e6-4c27-a129-837b37e1cf21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "dfd46c3a-0151-4fe2-9245-6a5d05c3f552",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67607f5e-b1e6-4c27-a129-837b37e1cf21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d76bed0c-db16-420d-8883-7f30b9a72017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67607f5e-b1e6-4c27-a129-837b37e1cf21",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "5930cd2c-17a2-4827-8f48-119143d339ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "5b86d2ba-16ad-4d38-a29e-7749990f70be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5930cd2c-17a2-4827-8f48-119143d339ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c2da351-a29b-428a-8fab-15833a757459",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5930cd2c-17a2-4827-8f48-119143d339ad",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "f5676bb2-371c-4973-a96a-830b99c7e57f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "27a82891-cfcb-4378-861f-3f89c6f72bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5676bb2-371c-4973-a96a-830b99c7e57f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22376e9e-0e91-41e0-a396-deceecdb4b08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5676bb2-371c-4973-a96a-830b99c7e57f",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "bccf8957-f067-4fc8-92e2-eb1f53a7bc1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "7f97e67e-8d31-4565-8a13-9ff1c15c06ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bccf8957-f067-4fc8-92e2-eb1f53a7bc1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65b3ac7d-2250-463b-a3be-697b63427efb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bccf8957-f067-4fc8-92e2-eb1f53a7bc1f",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        },
        {
            "id": "2cfe5058-8d9d-4bed-9614-b3a1b5644105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "compositeImage": {
                "id": "cf2dbd07-2c0c-4e67-b01b-62bfe4775312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cfe5058-8d9d-4bed-9614-b3a1b5644105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "524da066-d959-4e55-bf04-8a52668da716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cfe5058-8d9d-4bed-9614-b3a1b5644105",
                    "LayerId": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2916ceb1-c70a-4598-9be6-0a9edb2dccd7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97902785-dcf2-44eb-babc-8f1247621de3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}