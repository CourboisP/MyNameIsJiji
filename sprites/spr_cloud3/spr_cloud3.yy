{
    "id": "454e69d5-870f-4a66-81ce-a1758d90f71e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloud3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb50d5c0-8227-466f-8c9b-46d7ddcb7e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "454e69d5-870f-4a66-81ce-a1758d90f71e",
            "compositeImage": {
                "id": "4000402c-bca4-485d-9fc8-8fdd9bcee355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb50d5c0-8227-466f-8c9b-46d7ddcb7e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c540ae39-b4bf-4d0a-8a91-d52af098b201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb50d5c0-8227-466f-8c9b-46d7ddcb7e8e",
                    "LayerId": "75aafec0-7b81-4a0c-bb0d-4a2a0e574676"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "75aafec0-7b81-4a0c-bb0d-4a2a0e574676",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "454e69d5-870f-4a66-81ce-a1758d90f71e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}