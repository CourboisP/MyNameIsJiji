{
    "id": "0218f302-87d7-4acb-80c8-fabe7804277a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ligne",
    "eventList": [
        {
            "id": "a8d7da1d-3742-482d-b979-13c645cc72cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0218f302-87d7-4acb-80c8-fabe7804277a"
        },
        {
            "id": "3466fac9-0419-460f-969d-f98751d81617",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0218f302-87d7-4acb-80c8-fabe7804277a"
        },
        {
            "id": "5c1d8206-63a6-420e-b1d1-4c14e3e37dbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 5,
            "m_owner": "0218f302-87d7-4acb-80c8-fabe7804277a"
        },
        {
            "id": "809d310d-044c-45f7-9a46-da1ebbd823a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2a4f8d09-1116-49af-8c9b-878bfac2abad",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0218f302-87d7-4acb-80c8-fabe7804277a"
        },
        {
            "id": "413ac83c-9229-4a6e-8318-485361db1804",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "03b3a6be-2614-433c-8b8d-fca0536e3481",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0218f302-87d7-4acb-80c8-fabe7804277a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1225a6ef-9874-432a-92d4-6b7550bfbfbe",
    "visible": true
}