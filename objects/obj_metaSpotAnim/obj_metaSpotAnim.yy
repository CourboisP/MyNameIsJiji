{
    "id": "d2c52f84-e7cb-496b-84d2-0df5dfa96f7d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_metaSpotAnim",
    "eventList": [
        {
            "id": "679d9dcf-9969-4a15-bc1c-b4cc0e453539",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d2c52f84-e7cb-496b-84d2-0df5dfa96f7d"
        },
        {
            "id": "e9d06410-e0a2-4b4b-b5ce-ff6970e0e331",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d2c52f84-e7cb-496b-84d2-0df5dfa96f7d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}