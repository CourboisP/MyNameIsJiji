{
    "id": "83f5f38e-9927-41e9-af3f-83bd1bbb80bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_empty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da29a9e4-96bc-457d-9b3d-751b0ff8f169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83f5f38e-9927-41e9-af3f-83bd1bbb80bb",
            "compositeImage": {
                "id": "246aea6e-04af-4831-873c-0377136d29f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da29a9e4-96bc-457d-9b3d-751b0ff8f169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5c5ac73-6fd6-40ef-b56d-058841dc80d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da29a9e4-96bc-457d-9b3d-751b0ff8f169",
                    "LayerId": "0e46358c-9111-44ba-a86e-9ed48b9b9d75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0e46358c-9111-44ba-a86e-9ed48b9b9d75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83f5f38e-9927-41e9-af3f-83bd1bbb80bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}