{
    "id": "43561a61-e717-4b5c-bd30-b5e1695dc2d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 128,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4da5b9e2-c94d-4e77-b1d6-2c7cf75c74d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43561a61-e717-4b5c-bd30-b5e1695dc2d7",
            "compositeImage": {
                "id": "925e8c72-9407-4411-9dbc-78bddc7dd8d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4da5b9e2-c94d-4e77-b1d6-2c7cf75c74d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a55c43ee-b102-4ce8-93d4-0977e475f4dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4da5b9e2-c94d-4e77-b1d6-2c7cf75c74d1",
                    "LayerId": "76cb632f-8eca-4543-9b03-7d8b15d382ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "76cb632f-8eca-4543-9b03-7d8b15d382ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43561a61-e717-4b5c-bd30-b5e1695dc2d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}