{
    "id": "8445e75e-ecad-43d9-9e21-c43e967d6a8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backgroundWireAngle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 130,
    "bbox_right": 383,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aea68c07-d489-4dde-a2bc-b9284845bd77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8445e75e-ecad-43d9-9e21-c43e967d6a8e",
            "compositeImage": {
                "id": "30acbc72-cdaf-4f0b-9a03-62cda894ffa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea68c07-d489-4dde-a2bc-b9284845bd77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c46032-fa79-411e-827b-667dac180968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea68c07-d489-4dde-a2bc-b9284845bd77",
                    "LayerId": "5f182e57-b787-4161-956d-3b8015f607a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5f182e57-b787-4161-956d-3b8015f607a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8445e75e-ecad-43d9-9e21-c43e967d6a8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 192,
    "yorig": 64
}