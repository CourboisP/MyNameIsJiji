{
    "id": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wave2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c81c10ca-b272-446a-91ae-693e7142496f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "05d05df5-80fa-4c90-88ed-9ce915a9b142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c81c10ca-b272-446a-91ae-693e7142496f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "935a2e2c-afb6-453b-9f5e-3392c52df6b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c81c10ca-b272-446a-91ae-693e7142496f",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "51057f7f-a1c1-40be-8ec8-880e424cca84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "4986bcc2-57f6-49b4-b063-ec577f15f902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51057f7f-a1c1-40be-8ec8-880e424cca84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c59d225-de96-4a1a-8a2a-e342434c22b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51057f7f-a1c1-40be-8ec8-880e424cca84",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "61921d31-7c87-4550-a1e2-9767102365d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "827ff103-bb6d-4e5e-964b-37a781fee590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61921d31-7c87-4550-a1e2-9767102365d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "055b0f6d-1d74-4340-94de-d07a12314fc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61921d31-7c87-4550-a1e2-9767102365d1",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "c05b28b0-ab45-4d7f-81ea-3e20d0322d45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "81104980-9e70-453f-a53f-e70af6fd7847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c05b28b0-ab45-4d7f-81ea-3e20d0322d45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efeba497-eee6-4e66-9a07-cbcbc976a4ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c05b28b0-ab45-4d7f-81ea-3e20d0322d45",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "2d6f4677-2ab0-4a7d-9a72-3e795f6088ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "6da37024-240d-4c89-8587-695e12c41a4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d6f4677-2ab0-4a7d-9a72-3e795f6088ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b612946-c00f-48bb-8675-46d93fac0a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d6f4677-2ab0-4a7d-9a72-3e795f6088ee",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "3b145a5e-9bbb-4346-9e0f-e14780e3417f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "d0787fc7-b3bc-4b7f-af18-01faf5a13298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b145a5e-9bbb-4346-9e0f-e14780e3417f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fdd889e-e347-447d-aa03-ddd281bd2c50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b145a5e-9bbb-4346-9e0f-e14780e3417f",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "4dccd95d-a40d-4c23-a07d-08a0a0fdd956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "259503c4-ea98-4c35-8f78-62719a2207b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dccd95d-a40d-4c23-a07d-08a0a0fdd956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecf4edb9-989b-44a5-a586-c9d56e2c1092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dccd95d-a40d-4c23-a07d-08a0a0fdd956",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "b5fda171-61ef-4eca-bb91-bb4d989aa2d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "d2e4b57f-f35d-4202-a0fa-94133976efc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5fda171-61ef-4eca-bb91-bb4d989aa2d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14fd93f4-1281-4653-81df-7c12447457c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5fda171-61ef-4eca-bb91-bb4d989aa2d0",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "798c6e11-8e3f-44df-8408-88c69bf018dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "6b13d909-4cfb-4a23-bb93-3a641d4b3953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "798c6e11-8e3f-44df-8408-88c69bf018dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40636213-de7a-44c4-8fe0-ab7596e9bd22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "798c6e11-8e3f-44df-8408-88c69bf018dc",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "936eec8e-ab39-4293-9080-ceeee9da73ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "0b56b0a7-9c34-4940-a088-1a6e784cdba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "936eec8e-ab39-4293-9080-ceeee9da73ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc2388ae-4867-43b3-92a8-f5bbe787287d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "936eec8e-ab39-4293-9080-ceeee9da73ae",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "f2c74503-a6ee-4a79-89c7-9f05c7554e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "cee2fa35-08ab-4560-b583-64dd38250dd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c74503-a6ee-4a79-89c7-9f05c7554e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00be6f3d-29d3-452f-b567-c923710dd8d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c74503-a6ee-4a79-89c7-9f05c7554e70",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        },
        {
            "id": "aea831a7-bd1f-4a29-9d04-ac7c9a5e9bf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "compositeImage": {
                "id": "84c478de-b53f-4ef1-9a38-0acaa355bb05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea831a7-bd1f-4a29-9d04-ac7c9a5e9bf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b29b6bb9-68b9-4e7b-ad2e-07454aed335d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea831a7-bd1f-4a29-9d04-ac7c9a5e9bf6",
                    "LayerId": "8752ee50-f334-4dc1-83b7-17e10b406679"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8752ee50-f334-4dc1-83b7-17e10b406679",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d70e0570-5dcf-4ab6-ad83-a379658ee23c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}