{
    "id": "a37eb1f6-13b2-4ad3-a7a9-6c761a787f21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 957,
    "bbox_left": 4,
    "bbox_right": 1007,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6554492f-e8d6-4eeb-83fa-52ac23f1858d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a37eb1f6-13b2-4ad3-a7a9-6c761a787f21",
            "compositeImage": {
                "id": "5704af83-6d83-48e2-add2-04bc8b7fbd6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6554492f-e8d6-4eeb-83fa-52ac23f1858d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323d0beb-3421-4e82-b7d0-d06a08153e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6554492f-e8d6-4eeb-83fa-52ac23f1858d",
                    "LayerId": "60edc5a8-8a1d-4e80-b1ce-f790fb4de5a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "60edc5a8-8a1d-4e80-b1ce-f790fb4de5a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a37eb1f6-13b2-4ad3-a7a9-6c761a787f21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}