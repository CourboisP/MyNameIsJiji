{
    "id": "200cb56f-95fb-410f-8c6c-6c0495a382ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ordiBord",
    "eventList": [
        {
            "id": "790cd784-64af-456a-a22c-42895710d363",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "200cb56f-95fb-410f-8c6c-6c0495a382ef"
        },
        {
            "id": "a892cad9-3e00-4177-b1e1-f06bf299fc2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "200cb56f-95fb-410f-8c6c-6c0495a382ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d962eaf6-46ba-4913-906a-eef78956e935",
    "visible": true
}