{
    "id": "f33fc188-5f5a-4da5-aaa7-66827ef0d159",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hublot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e2e8856-5c38-48b5-abf8-e330d062eee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f33fc188-5f5a-4da5-aaa7-66827ef0d159",
            "compositeImage": {
                "id": "8fc468be-e944-4a62-80e9-c565d26e22df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e2e8856-5c38-48b5-abf8-e330d062eee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fb2600b-71cc-4eca-b20b-24e1ec79b84c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2e8856-5c38-48b5-abf8-e330d062eee6",
                    "LayerId": "82f1e5c4-6882-48f0-b6ac-4010e456b801"
                }
            ]
        },
        {
            "id": "5f41055f-5ad5-4a4b-875b-e1e9ee7f6c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f33fc188-5f5a-4da5-aaa7-66827ef0d159",
            "compositeImage": {
                "id": "e3bb3011-f37e-491f-8381-95b74e743f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f41055f-5ad5-4a4b-875b-e1e9ee7f6c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ff52661-9654-4118-a7ab-3a92c66197fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f41055f-5ad5-4a4b-875b-e1e9ee7f6c73",
                    "LayerId": "82f1e5c4-6882-48f0-b6ac-4010e456b801"
                }
            ]
        },
        {
            "id": "9d7a3915-36d0-473e-a532-2bd87af439bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f33fc188-5f5a-4da5-aaa7-66827ef0d159",
            "compositeImage": {
                "id": "f63d8c6e-9503-4d8c-83d0-d737e9b75750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d7a3915-36d0-473e-a532-2bd87af439bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b24be7c-1501-40f1-9230-6217266752fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d7a3915-36d0-473e-a532-2bd87af439bf",
                    "LayerId": "82f1e5c4-6882-48f0-b6ac-4010e456b801"
                }
            ]
        },
        {
            "id": "caa93457-ee7f-48b0-9f99-3720d271d34d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f33fc188-5f5a-4da5-aaa7-66827ef0d159",
            "compositeImage": {
                "id": "a3b61de5-8a01-4791-85a6-aa79abfaa9b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caa93457-ee7f-48b0-9f99-3720d271d34d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "145bf3e9-87ef-4c57-978f-d357ea685f50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caa93457-ee7f-48b0-9f99-3720d271d34d",
                    "LayerId": "82f1e5c4-6882-48f0-b6ac-4010e456b801"
                }
            ]
        },
        {
            "id": "6e468b63-ed36-4867-8ce7-ffb7d3699386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f33fc188-5f5a-4da5-aaa7-66827ef0d159",
            "compositeImage": {
                "id": "72c20f0f-6f59-42a2-8c5d-e8c130498521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e468b63-ed36-4867-8ce7-ffb7d3699386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce673152-efcd-469c-a2bd-f42fcce48652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e468b63-ed36-4867-8ce7-ffb7d3699386",
                    "LayerId": "82f1e5c4-6882-48f0-b6ac-4010e456b801"
                }
            ]
        },
        {
            "id": "e4b1af7a-7df8-4986-a0ae-277a4561944e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f33fc188-5f5a-4da5-aaa7-66827ef0d159",
            "compositeImage": {
                "id": "c2886d45-3457-4c73-a538-7e617c403edb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4b1af7a-7df8-4986-a0ae-277a4561944e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c558b3fc-f405-4295-a4d2-dc444084ce28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4b1af7a-7df8-4986-a0ae-277a4561944e",
                    "LayerId": "82f1e5c4-6882-48f0-b6ac-4010e456b801"
                }
            ]
        },
        {
            "id": "5f8ee138-623e-4f59-9514-d20249f073ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f33fc188-5f5a-4da5-aaa7-66827ef0d159",
            "compositeImage": {
                "id": "842027e1-32d8-4591-8161-0f83147d0e0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f8ee138-623e-4f59-9514-d20249f073ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94a8c361-3b04-4749-9212-c4b3abce54ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f8ee138-623e-4f59-9514-d20249f073ba",
                    "LayerId": "82f1e5c4-6882-48f0-b6ac-4010e456b801"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "82f1e5c4-6882-48f0-b6ac-4010e456b801",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f33fc188-5f5a-4da5-aaa7-66827ef0d159",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}