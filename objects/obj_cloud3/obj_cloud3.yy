{
    "id": "05252c6f-4b4c-4e52-8076-3894aa605e6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cloud3",
    "eventList": [
        {
            "id": "2b134271-c70f-4b0c-9990-063d9854bbd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "05252c6f-4b4c-4e52-8076-3894aa605e6b"
        },
        {
            "id": "0671f3ac-3d1a-4307-87f6-d3ec4311b967",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "05252c6f-4b4c-4e52-8076-3894aa605e6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "454e69d5-870f-4a66-81ce-a1758d90f71e",
    "visible": true
}