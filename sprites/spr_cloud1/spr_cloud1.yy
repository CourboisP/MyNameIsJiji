{
    "id": "564c60ff-49d6-46d8-9bb3-5fcf1a39fd30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloud1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18c0a1ed-14b4-46cd-a38f-2903b9beb761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "564c60ff-49d6-46d8-9bb3-5fcf1a39fd30",
            "compositeImage": {
                "id": "2534ff2a-859e-4097-ba82-00eb38116def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18c0a1ed-14b4-46cd-a38f-2903b9beb761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9989ce9-25b7-42ed-b527-3c8132ce0685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18c0a1ed-14b4-46cd-a38f-2903b9beb761",
                    "LayerId": "7360099b-9088-4818-9b6d-737d153b4b76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7360099b-9088-4818-9b6d-737d153b4b76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "564c60ff-49d6-46d8-9bb3-5fcf1a39fd30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}