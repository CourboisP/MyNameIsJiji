{
    "id": "c569ac2e-5689-4f36-97b2-d91b760d17d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SpotMorseMotsOK",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c03b9b52-80f7-46fc-98ff-0cfc33c88348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c569ac2e-5689-4f36-97b2-d91b760d17d2",
            "compositeImage": {
                "id": "1801fbbb-87a8-4026-bd57-1606fbde4c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c03b9b52-80f7-46fc-98ff-0cfc33c88348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fef05c76-a950-4674-bb10-3556d389e6de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c03b9b52-80f7-46fc-98ff-0cfc33c88348",
                    "LayerId": "2ee7b46c-d492-4b8c-92bf-475f6cd3f874"
                }
            ]
        },
        {
            "id": "f5b7a464-f84a-42df-a5fb-cbadc3d5d561",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c569ac2e-5689-4f36-97b2-d91b760d17d2",
            "compositeImage": {
                "id": "d15be007-3804-4ec8-9173-92e40a93f6c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5b7a464-f84a-42df-a5fb-cbadc3d5d561",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c4aa7e1-7596-4569-8038-56554aae1780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5b7a464-f84a-42df-a5fb-cbadc3d5d561",
                    "LayerId": "2ee7b46c-d492-4b8c-92bf-475f6cd3f874"
                }
            ]
        },
        {
            "id": "8164521e-1db3-4b9a-a7a1-3f5ad86c885f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c569ac2e-5689-4f36-97b2-d91b760d17d2",
            "compositeImage": {
                "id": "a252fbd4-1ed7-4abf-80c7-972effcc7495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8164521e-1db3-4b9a-a7a1-3f5ad86c885f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4a68691-3410-45f6-a7e0-6ae36958ad6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8164521e-1db3-4b9a-a7a1-3f5ad86c885f",
                    "LayerId": "2ee7b46c-d492-4b8c-92bf-475f6cd3f874"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2ee7b46c-d492-4b8c-92bf-475f6cd3f874",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c569ac2e-5689-4f36-97b2-d91b760d17d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}