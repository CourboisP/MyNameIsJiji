{
    "id": "74b048ab-e1a8-4e6c-ac17-c048b33a8ae4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_amecon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 32,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99457c8f-2010-45f3-ad59-fe9af000892c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b048ab-e1a8-4e6c-ac17-c048b33a8ae4",
            "compositeImage": {
                "id": "a375ad8f-7b65-492b-ac41-5474ce09e1f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99457c8f-2010-45f3-ad59-fe9af000892c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c902cdf1-6602-4cef-abba-64e3b60a6652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99457c8f-2010-45f3-ad59-fe9af000892c",
                    "LayerId": "3ddae708-38a3-43b0-8238-66ede1ebb5ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3ddae708-38a3-43b0-8238-66ede1ebb5ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74b048ab-e1a8-4e6c-ac17-c048b33a8ae4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}