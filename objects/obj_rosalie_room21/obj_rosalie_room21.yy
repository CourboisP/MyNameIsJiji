{
    "id": "babf0135-bf0f-4084-b86b-ca46d9719216",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rosalie_room21",
    "eventList": [
        {
            "id": "b0a2e880-5d35-48f3-b8a6-f2bb17aac942",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "babf0135-bf0f-4084-b86b-ca46d9719216"
        },
        {
            "id": "b7fa776a-b984-4066-bd36-da1bc9528a29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "babf0135-bf0f-4084-b86b-ca46d9719216"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "16ef0e67-6d5a-4cdb-b9c2-e1c55de739bd",
    "visible": true
}