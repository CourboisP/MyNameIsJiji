{
    "id": "10927388-6209-46c2-bf38-98b4d1355908",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_alarm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8ec047d-f22a-458c-a3e2-a2b3eb7c3b76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10927388-6209-46c2-bf38-98b4d1355908",
            "compositeImage": {
                "id": "fce12b1c-9bd7-4591-a86c-a898d2b5adb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8ec047d-f22a-458c-a3e2-a2b3eb7c3b76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac7a3d48-5c54-4901-bde7-9e17554326c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8ec047d-f22a-458c-a3e2-a2b3eb7c3b76",
                    "LayerId": "0dbf1f0b-e490-442a-8636-6eba34f186fc"
                }
            ]
        },
        {
            "id": "a342dced-d938-47ba-84b9-db2d5113bd54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10927388-6209-46c2-bf38-98b4d1355908",
            "compositeImage": {
                "id": "4c3d3ea9-ccb4-427f-a125-6e5fa6746fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a342dced-d938-47ba-84b9-db2d5113bd54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2091946e-fc4f-4370-9d49-90c3b876695f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a342dced-d938-47ba-84b9-db2d5113bd54",
                    "LayerId": "0dbf1f0b-e490-442a-8636-6eba34f186fc"
                }
            ]
        },
        {
            "id": "91b25ac5-7c98-4372-b2d4-7da50e519536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10927388-6209-46c2-bf38-98b4d1355908",
            "compositeImage": {
                "id": "f469c6f4-c7bc-457b-b52d-6c1906d3c438",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91b25ac5-7c98-4372-b2d4-7da50e519536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ea53e3a-1caf-47c2-b234-b5091d534451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91b25ac5-7c98-4372-b2d4-7da50e519536",
                    "LayerId": "0dbf1f0b-e490-442a-8636-6eba34f186fc"
                }
            ]
        },
        {
            "id": "97a1b7e7-29d6-4490-9dee-783455d72b81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10927388-6209-46c2-bf38-98b4d1355908",
            "compositeImage": {
                "id": "a6a76975-efec-4611-8c73-3e35bc4e4401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a1b7e7-29d6-4490-9dee-783455d72b81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65515dde-519c-481a-a7d3-6f8196ce706c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a1b7e7-29d6-4490-9dee-783455d72b81",
                    "LayerId": "0dbf1f0b-e490-442a-8636-6eba34f186fc"
                }
            ]
        },
        {
            "id": "62a18466-4474-4820-9202-f549bb03459f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10927388-6209-46c2-bf38-98b4d1355908",
            "compositeImage": {
                "id": "c30bb109-e2cf-4bab-bf6f-97b1e6a4d524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a18466-4474-4820-9202-f549bb03459f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0bc21e7-2196-49fb-9ae1-32eb807617ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a18466-4474-4820-9202-f549bb03459f",
                    "LayerId": "0dbf1f0b-e490-442a-8636-6eba34f186fc"
                }
            ]
        },
        {
            "id": "0e05cb2d-933e-4ace-96d2-21fff1f9d288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10927388-6209-46c2-bf38-98b4d1355908",
            "compositeImage": {
                "id": "937c1931-9a68-4d95-81b3-89d3a5913bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e05cb2d-933e-4ace-96d2-21fff1f9d288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50abc298-da8b-4fc7-8c69-e016aa010670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e05cb2d-933e-4ace-96d2-21fff1f9d288",
                    "LayerId": "0dbf1f0b-e490-442a-8636-6eba34f186fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0dbf1f0b-e490-442a-8636-6eba34f186fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10927388-6209-46c2-bf38-98b4d1355908",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}