{
    "id": "c0c9f1f3-4712-443a-861b-56c34c2bed09",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fadeNext2",
    "eventList": [
        {
            "id": "106e75a3-fd85-4956-8c1e-93777bdfad7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0c9f1f3-4712-443a-861b-56c34c2bed09"
        },
        {
            "id": "34435f2d-6d3e-48ad-8d44-66e145844924",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c0c9f1f3-4712-443a-861b-56c34c2bed09"
        },
        {
            "id": "5d362334-aab1-4402-8521-2b3b4ad7e573",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c0c9f1f3-4712-443a-861b-56c34c2bed09"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}