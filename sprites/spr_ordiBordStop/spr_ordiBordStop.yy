{
    "id": "d962eaf6-46ba-4913-906a-eef78956e935",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ordiBordStop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 26,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aab5b501-d4e8-4b6f-b0c3-7629cbebf1ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d962eaf6-46ba-4913-906a-eef78956e935",
            "compositeImage": {
                "id": "cfdd37a1-c20c-448d-9eed-c5dd9ceccb9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab5b501-d4e8-4b6f-b0c3-7629cbebf1ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b82f1f70-2c3e-447a-b146-9a9469b9f5e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab5b501-d4e8-4b6f-b0c3-7629cbebf1ab",
                    "LayerId": "75cb8abc-311e-450c-b4f0-838cf61b19d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "75cb8abc-311e-450c-b4f0-838cf61b19d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d962eaf6-46ba-4913-906a-eef78956e935",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}