{
    "id": "9e4a75f5-66c0-4169-8527-194fe2bfe778",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 947,
    "bbox_left": 4,
    "bbox_right": 1007,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9631bce5-31a1-4ff4-819d-9b58d7853685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e4a75f5-66c0-4169-8527-194fe2bfe778",
            "compositeImage": {
                "id": "6226264f-a209-4e19-8f50-b43bb94dd632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9631bce5-31a1-4ff4-819d-9b58d7853685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52d12ea8-d9ed-48eb-8356-76e6fd47b1a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9631bce5-31a1-4ff4-819d-9b58d7853685",
                    "LayerId": "8f425bcc-1ace-4ff9-81bb-bada0fb832de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "8f425bcc-1ace-4ff9-81bb-bada0fb832de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e4a75f5-66c0-4169-8527-194fe2bfe778",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}