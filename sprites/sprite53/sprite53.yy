{
    "id": "343136bd-2b90-48b1-be57-36359f73dbaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite53",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e84bb815-1352-4e6b-9338-dcb711b84382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "343136bd-2b90-48b1-be57-36359f73dbaf",
            "compositeImage": {
                "id": "65330630-1e84-4664-a6c6-5ea82eb17dd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84bb815-1352-4e6b-9338-dcb711b84382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78433e51-8ac4-4e3d-9305-e363159f4650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84bb815-1352-4e6b-9338-dcb711b84382",
                    "LayerId": "aa1ef7bc-28e3-428b-a9b1-e31694c063b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "aa1ef7bc-28e3-428b-a9b1-e31694c063b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "343136bd-2b90-48b1-be57-36359f73dbaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}